##Intro
This packages adds helper methods to create lock objects based on keysets

##Package
The package installs the following:

* A dll with the helper methods in case of the binary package
* A helper class in case of the -code- package

##Methods
| Method | Description
|---|---
| GetLockObject(string key) | Use this method to obtain a lockobject bound to a unique key (like a filepath to prevent simultaneous access or just some constant key). This can be used for general purpose, all calls with the same key will be blocked.
| GetLockObject(params string[] dependencies) | Use this method to obtain a lockobject bound to a unique key (like a filepath to prevent simultaneous access or just some constant key). This can be used for general purpose, all calls with the same key will be blocked. All dependencies are used to generate a unique key for this lockobject. Use simple types only!
| GetLockObjectForThisMethod(params object[] dependencies) | Use this method to obtain a lockobject bound to the calling method and optional dependencies, all calls with the same key will be blocked. This can be used for general purpose, all calls with the same key will be blocked. All additional dependencies are used to generate a unique key for this lockobject. Use simple types only!
| GetLockObjectForThisClass(params object[] dependencies) |  Use this method to obtain a lockobject bound to the calling class and optional dependencies, all calls with the same key will be blocked. This can be used for general purpose, all calls with the same key will be blocked. All additional dependencies are used to generate a unique key for this lockobject. Use simple types only!
