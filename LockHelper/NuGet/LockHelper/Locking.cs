﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace TamTam.NuGet.LockHelper
{
    public static class Locking
    {
        private static object _lockSetter = new object();
        private static Dictionary<string, object> _lockObjectList = new Dictionary<string, object>();
        private static object GetLockObject(string key)
        {
            if (_lockObjectList.ContainsKey(key))
            {
                return _lockObjectList[key];
            }
            else
            {
                lock (_lockSetter)
                {
                    if (!_lockObjectList.ContainsKey(key))
                    {
                        _lockObjectList.Add(key, new object());
                    }
                    return _lockObjectList[key];
                }
            }
        }

        /// <summary>
        /// Use this method to obtain a lockobject bound to a unique key (like a filepath to prevent simultaneous access or just some constant key). 
        /// This can be used for general purpose, all calls with the same key will be blocked. 
        /// </summary>
        /// <param name="dependencies">All dependencies used to generate a unique key for this lockobject. Use simple types only!</param>
        /// <returns>The lockobject bound to the provided key</returns>
        public static object GetLockObject(params string[] dependencies)
        {
            if (dependencies.Length < 1)
                throw new ArgumentException("At least one dependency must be specified");

            string key = string.Join(".", dependencies);

            return GetLockObject(key);
        }

        /// <summary>
        /// Use this method to obtain a lockobject bound to the calling method and optional dependencies, all calls with the same key will be blocked.
        /// This can be used for general purpose, all calls with the same key will be blocked. 
        /// </summary>
        /// <param name="dependencies">All additinal dependencies used to generate a unique key for this lockobject. Use simple types only!</param>
        /// <returns>The lockobject bound to the provided key</returns>
        public static object GetLockObjectForThisMethod(params object[] dependencies)
        {
            StackTrace stackTrace = new StackTrace();
            var method = stackTrace.GetFrame(1).GetMethod();
            return GetLockObject(method.DeclaringType.FullName, method.Name);
        }

        /// <summary>
        /// Use this method to obtain a lockobject bound to the calling class and optional dependencies, all calls with the same key will be blocked.
        /// This can be used for general purpose, all calls with the same key will be blocked. 
        /// </summary>
        /// <param name="dependencies">All additinal dependencies used to generate a unique key for this lockobject. Use simple types only!</param>
        /// <returns>The lockobject bound to the provided key</returns>
        public static object GetLockObjectForThisClass(params object[] dependencies)
        {
            StackTrace stackTrace = new StackTrace();
            var method = stackTrace.GetFrame(1).GetMethod();
            return GetLockObject(method.DeclaringType.FullName);
        }
    }
}
